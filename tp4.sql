CREATE OR REPLACE Trigger checksal_ly
BEFORE UPDATE of job on emp
For each row
DECLARE
    min_sal emp.sal%type;
    max_sal emp.sal%type;
Begin
    if (:old.job != 'PRESIDENT') then
        SELECT lsal, hsal INTO min_sal, max_sal FROM SalIntervalle_ly WHERE job = :new.job;
        :new.sal := GREATEST(min_sal, LEAST(max_sal, :old.sal+100));
    End if;
End;
/

CREATE OR REPLACE Package package_tp4_ly AS
    TYPE EmpType IS RECORD (empno emp.empno%type, ename emp.ename%type);
    Cursor emp_par_dep_ly (x_deptno emp.deptno%type) return EmpType;
    Procedure raisesalary_ly (x_emp_id emp.empno%type, x_amount emp.sal%type);
    Procedure afficher_emp_ly (x_deptno emp.deptno%type);
END package_tp4_ly;
/

CREATE OR REPLACE Package Body package_tp4_ly AS
    Cursor emp_par_dep_ly (x_deptno emp.deptno%type) return EmpType IS
        SELECT empno, ename FROM emp WHERE deptno = x_deptno; 
    Procedure raisesalary_ly (x_emp_id emp.empno%type, x_amount emp.sal%type) IS
        new_sal emp.sal%type;
        empjob emp.job%type;
        Cursor c IS SELECT job, sal FROM emp WHERE empno=x_emp_id;
        Begin
            OPEN c;
            FETCH c INTO empjob, new_sal;
            CLOSE c;
            new_sal := new_sal + x_amount;
            if (salok_ly(empjob, new_sal) = 1) then
                UPDATE emp
                SET sal = new_sal
                WHERE empno = x_emp_id;
            else
                raise_application_error(-20001,'salary is too high');
            end if;
        End raisesalary_ly;
    Procedure afficher_emp_ly (x_deptno emp.deptno%type) IS
        Cursor c IS SELECT empno, ename FROM emp WHERE deptno = x_deptno;
        Begin
            FOR rec_c in c
            LOOP
                dbms_output.put_line('numero employe : '|| rec_c.empno || '     nom employe : ' || rec_c.ename);
            END LOOP;
        end afficher_emp_ly;

END package_tp4_ly;
/

CREATE OR REPLACE Trigger raise_ly
BEFORE UPDATE of sal on emp
For each row 
Begin
    if (:old.sal > :new.sal) then
        raise_application_error(-20120, 'le salaire ne peut pas diminuer');
    End if;
End;
/

Declencheur cree.
UPDATE emp SET sal = sal - 100	WHERE ename = 'ADAMS'
       *
ERREUR a la ligne 1 :
ORA-20120: le salaire ne peut pas diminuer
ORA-06512: a "SILY.RAISE_LY", ligne 3
ORA-04088: erreur lors d'execution du declencheur 'SILY.RAISE_LY'


UPDATE emp SET sal = sal - 100
       *
ERREUR a la ligne 1 :
ORA-20120: le salaire ne peut pas diminuer
ORA-06512: a "SILY.RAISE_LY", ligne 3
ORA-04088: erreur lors d'execution du declencheur 'SILY.RAISE_LY'


CREATE OR REPLACE Trigger numdept_ly
BEFORE UPDATE of deptno on emp
For each row 
Begin
    if (:new.deptno > 69 OR :new.deptno < 61) then
        raise_application_error(-20121, 'le departement doit etre dans [61,69]');
    End if;
End;
/

CREATE TABLE STATS_ly (TypeMaj varchar2(10), NbMaj number, Date_derniere_Maj date);

insert into STATS_ly values ('INSERT', 0, NULL);
insert into STATS_ly values ('DELETE', 0, NULL);
insert into STATS_ly values ('UPDATE', 0, NULL);

CREATE OR REPLACE Trigger update_stats_ly
AFTER UPDATE OR INSERT OR DELETE on emp
For each row 
Begin
    if INSERTING then
    UPDATE STATS_ly SET NbMaj = NbMaj+1, Date_derniere_Maj = SYSDATE WHERE TypeMaj='INSERT';
    End if;
    if UPDATING then
    UPDATE STATS_ly SET NbMaj = NbMaj+1, Date_derniere_Maj = SYSDATE WHERE TypeMaj='UPDATE';
    End if;
    if DELETING then
    UPDATE STATS_ly SET NbMaj = NbMaj+1, Date_derniere_Maj = SYSDATE WHERE TypeMaj='DELETE';
    End if;
End;
/